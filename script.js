class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }
  get name() {
    return this_name;
  }
  get age() {
    return this_age;
  }
  get salary() {
    return this_salary;
  }
  set name(value) {
    this_name = value;
  }
  set age(value) {
    this._age = value;
  }
  set salary(value) {
    this._salary = value;
  }
}
class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.lang = lang;
  }
  get salary() {
    return this._salary * 3;
  }
}
const bob = new Programmer("Bob", 27, 25000, "English");
const nikol = new Programmer("Nikol", 23, 23000, "China");
console.log(bob.salary);
console.log(bob);
console.log(nikol.salary);
console.log(nikol);
